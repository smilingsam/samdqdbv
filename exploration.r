
##---------------------------------
## ADS: Classifying Laboratory Data
## Bitbucket: ADS_NSSP
##---------------------------------

## Installation Function
mypak <- function(pkg){
  new.pkg <- pkg[!(pkg %in% installed.packages()[, "Package"])]
  if (length(new.pkg)) 
    install.packages(new.pkg, dependencies = TRUE)
  sapply(pkg, require, character.only = TRUE)
}

## Package List
packages  <- c("devtools", "here", "readr", "tidyverse", "odbc", "DBI", "dendextend",
               "tm", "tidytext", "proxy", "dbscan", "textmineR", "tokenizers", "plotly",
               "stringr", "stopwords", "caret", "tictoc", "corrplot", "cluster")


#mypak(packages)

## General Libraries 
library(devtools); library(here); library(tidyverse); library(here); library(readr); library(plotly)
library(odbc); library(DBI); library(proxy); library(dbscan); library(dplyr); library(qdap)
library(caret); library(tictoc); library(corrplot); library(cluster); library(readxl)
library(pander); library(widyr)

## Text Classification Libraries
library(dendextend); library(tm); library(tidytext); library(textmineR); library(tokenizers)
library(stringr); library(stopwords); library(qdap); library(igraph); library(ggraph)
library(SnowballC)

## Install Github based Packages#
#install_github("juliasilge/tidylo")
library(tidylo)

# here()

##------------------
## Feature Functions
##------------------

lab_hitter <- function(query){
  con <- dbConnect(odbc::odbc(), "LAB"); table <- dbGetQuery(con, query)
  return(table)
}

##--------------------
## Step I: Data Intake
##--------------------

set.seed(666) 

## Bring in reference data
chronic <- readxl::read_excel("~/ADS/ads_nssp/Copy of ORM_400_RFT_Chronic_Ailments BLV EDITS.xlsx")

## Bring in data
tic()
qry <- paste0("SELECT Top 1000000 * FROM [ADS_TESTDB].[dbo].[Lab_Result_Subset1]")
results <- lab_hitter(qry)
toc() ## 1 mil takes 2 minutes

## Remove NAs
results[is.na(results)] <- ""

## Randomly sample the data
ogresults <- dplyr::sample_n(results, 250000)

## Exclude chronic and uninformative RFTDs
chronic                             <- chronic %>% dplyr::filter(`Chronic Ailment` == 'X' | Useless == 'X')
comparables                         <- unique(chronic$Reason_For_Test_Description)[i]
results$Reason_For_Test_Description <- gsub(paste0("\\s(",paste(comparables, collapse="|"),")\\s"), " ", results$Reason_For_Test_Description)

## Create ID column 
ogresults$Calculated_ID <- paste0(ogresults$C_Accession_ID, ogresults$Result_Test_Code_Local) 
length(unique(ogresults$Calculated_ID))

## How many result codes are we working with? 
length(unique(ogresults$Result_Test_Code))

## Dropping Specialty_Code_Description for clustering. 
results <- ogresults %>% dplyr::select(Calculated_ID, 
                                       Ordered_Test_Description, 
                                       Ordered_Test_Description_Local, 
                                       Result_Test_Description,
                                       Result_Test_Description_Local,
                                       Reason_For_Test_Description)

## C_Accession_ID will be doc id. Text will be concatenated column. 
results$text <- paste0(results$Ordered_Test_Description, " ", 
                       results$Result_Test_Description_Local, " ",
                       results$Result_Test_Description, " ",
                       results$Ordered_Test_Description_Local, " ",
                       results$Reason_For_Test_Description)

## Check to see how diverse the data is
results <- distinct(results); nrow(results); length(unique(results$text))

##-----------------------
## Step II: Text Cleaning
##-----------------------

## Convert to lowercase for cleaning 
results$text <- tolower(results$text)

## Stem the column to remove plurals, derivatives
results$text <-SnowballC::wordStem(results$text, language = 'en')

## Create abbreviation and replacement vectors
abv  <- c("result 1", "rslt#1", "result 2", "rslt#2", "result 3", "rslt#3", "result 4", "rslt#4", 
          "hiv 1+2", "hiv 1", "hiv 2",
          "type 1", "type 2",
          "hsv 1", "hsv 2",
          "virus 1", "virus 2", "virus 3",
          "stage 1", "stage 2", "stage 3 ", "stage 4", "stage 5",
          "w/", ";", ",",
          "parainfluenza 1", "parainfluenza 2", "parainfluenza 3")
repl <- c("result", "result", "result", "result", "result", "result", "result", "result",
          "hiv_1+2", "hiv_1", "hiv_2",
          "type_1", "type_2",
          "hsv_1", "hsv_2",
          "virus_1", "virus_2", "virus_3",
          "stage_1", "stage_2", "stage_3", "stage_4", "stage_5",
          "with", " ", " ",
          "parainfluenza_1", "parainfluenza_2", "parainfluenza_3")
          
## Replace abbreviations (primarily to preserve numerics)
results$text <- qdap::replace_abbreviation(results$text, abv, repl)

## Define Lab stopwords - note that these are stemmed
lab_stops <- c("bacteria identified", "result", "site not specifi", "panel",
               "summary report (summary)", "drugs identified", "compliance drug analysis",
               "encounter for screening for infections with a prodminantly sexual mode of",
               "culture", "urine culture", "culture/type", "routine frequency",
               "dependenc uncompl", "encounter for general adult medical examination without abnormal findings",
               "encounter for supervision of other normal", 
               "encounter for screenings for", "unspecifi", "stool culture",
               "other fecal abnorm", "anaerobic and aerobic culture", 
               )

## Removing them first           
results$text <- gsub(paste0("\\s(",paste(lab_stops, collapse="|"),")\\s"), " ", results$text)

## Removing common stopwords next
results$text <- gsub(paste0("\\s(",paste(tm::stopwords(), collapse="|"),")\\s"), " ", results$text)

## Removing pure numerics
results$text <- gsub("\\b\\d+\\b", "", results$text)

## Trim whitespace produced from cleansing
results$text <- trimws(results$text, which = c("both"), whitespace = "[ \t\r\n]")

saveRDS(results, "results.rds"); gc()
results <- readRDS("~/bbaloughoa01/results.rds")

##-----------------------
## Step III: Tokenization
##-----------------------

## Tranform into tidytext tokens
lab_tib <- tibble(id = results$Calculated_ID, text = results$text) 
lab_tib <- lab_tib %>% unnest_tokens(word, text,
                                     token = "ngrams",
                                     n_min = 1, n = 2) %>% anti_join(stop_words) 